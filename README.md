Setări default:

- **100** de probleme;
- dificultate **50**;
- **nu** se afișează rezultatele corecte la final;

Primele două setări pot fi schimbate din cod, iar a treia înseamnă decomentarea câtorva linii. Nu cred că voi lucra mai mult de atât la cod.